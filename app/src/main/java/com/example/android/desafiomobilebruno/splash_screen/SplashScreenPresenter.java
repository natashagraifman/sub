package com.example.android.desafiomobilebruno.splash_screen;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.example.android.desafiomobilebruno.entity.EntidadeSocialList;
import com.example.android.desafiomobilebruno.network.api.AppApi;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 726574 on 24/03/18.
 */

public class SplashScreenPresenter {
    SplashScreenView splashScreenView;
    SplashScreenPresenter splashScreenPresenter;

    public SplashScreenPresenter(SplashScreenView splashScreenView) {
        this.splashScreenView = splashScreenView;
    }

    public void pegaSocialAutomatico(){
        //gera o atraso de 2 segundos para exibição da splash screen
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                //Utiliza o Retrofit para conectar-se ao servidor web
                //Caso haja dados eles são salvos e a lista é exibida
                //Caso contrário é verificado se há dados armazenados no dispostivo
                AppApi api = AppApi.getInstance();

                api.getSociais().enqueue(new Callback<EntidadeSocialList>() {
                    @Override
                    public void onResponse(Call<EntidadeSocialList> call, Response<EntidadeSocialList> response) {
                        EntidadeSocialList list = response.body();

                        if(list != null && list.getEntidadeSocialList() != null) {
                            splashScreenView.salvaDadosOffline(new Gson().toJson(list));
                            splashScreenView.exibeEntidadesSociais(list);
                        }else {
                            splashScreenView.trabalhaOffline();
                        }
                    }

                    @Override
                    public void onFailure(Call<EntidadeSocialList> call, Throwable t) {
                        splashScreenView.trabalhaOffline();
                        Log.e("network", t.toString());

                    }
                });
            }
        }, 2000);

    }
}
