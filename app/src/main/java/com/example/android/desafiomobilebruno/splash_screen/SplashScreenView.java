package com.example.android.desafiomobilebruno.splash_screen;

import com.example.android.desafiomobilebruno.entity.EntidadeSocialList;
import com.google.gson.Gson;

/**
 * Created by 726574 on 24/03/18.
 */

public interface SplashScreenView {
    void salvaDadosOffline(String jsonEntidadesSociais);
    void exibeEntidadesSociais(EntidadeSocialList entidadeSocialList);
    void trabalhaOffline();
}
